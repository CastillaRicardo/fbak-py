fbak
====
Simple UNIX/Linux backup script

Usage:
=============
```
$ fbak [options] TARGET
```

*Targets can be directories, or files.
Feel free to use wildcards*

Compilation and Installation:
=============================

To compile the python script into an executable, it is necessary to install nuitka (http://nuitka.net/)

On OSX, it is preferred to install nuitka through homebrew:
```
$ brew update && brew install nuitka
```

Once nuitka is installed, run
```
$ make
```
The makefile accepts extra nuitka flags. Read nuitka's manpage for the full list of accepted flags.

To install fbak to /usr/local/bin open a terminal and run
```
$ make install
```
*Remember to use `sudo` if necessary*

It is also possible to specify a specific folder where to install fbak under by specifying `DESTDIR`.
The command below would install fbak in /tmp/usr/local/bin/fbak
```
$ make DESTDIR=/tmp install
```

Install without compiling
========================
If you cannot or do not want to install nuitka to compile the program, you may use the command below
```
$ make altinstall
```

Uninstallation and Reinstallation:
=============

To uninstall fbak from the default location `/usr/local/bin` run
```
$ make deisntall
```

If fbak was installed on a different directory, it must be specified in `DESTDIR` similar to the installation process

To reinstall fbak, use
```
$ make reinstall
```
This command will deinstall the program, recompile if necessary, and install the new binary and man page.

If fbak was installed somewhere else than `/usr/local/bin/` it needs to be specified in `DESTDIR`

Clean:
======

To clean the development environment run
```
$ make clean
```

If you are developing fbak, and need to also clean all `*.BAK.*` files, run
```
$ make cleanall
```

Adding to OS X context menu
=============
Double click `fbak.workflow` and select install

You should now have an entry on the context menu (or under the Services submenu) with the name you gave the workflow

Adding to Linux context menu
=============
You will have to follow your desktop environment's action creation guide.

**XFCE:**

* Open your file manager
* Select `Edit` tab
* Select `Configure custom actions`
* Give it a descriptive name (e.g. Run fbak) and a description
* If you ran the install script, your command will look like `fbak -n %D %F`. If not, you will have to specifiy the absolute path to fbak
* Under `Appearance and Conditions`, make sure all selection options are checked and file pattern is `*`

**GNOME:**

* Use nautilus-actions
