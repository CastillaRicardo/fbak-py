# Makefile for fbak-py
# Maintainer: Ricardo Castilla

EXEC=fbak
NEXEC=fbak.exe
PREFIX=/usr/local
#NFLAGS are extra flags for nuitka

default: fbak

fbak: fbak.py
	nuitka --recurse-all $(NFLAGS) fbak.py
	@mv $(NEXEC) $(EXEC)

.PHONY: instman
instman: fbak.1
	@echo "Installing man page"
	cp fbak.1 /usr/share/man/man1/
	@echo "Compressing man page"
	gzip /usr/share/man/man1/fbak.1

.PHONY: install
install: default instman
	@echo "Installing fbak"
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $(EXEC) $(DESTDIR)$(PREFIX)/bin/$(EXEC)
	chmod 555 $(DESTDIR)$(PREFIX)/bin/$(EXEC)

.PHONY: altinstall
altinstall: fbak.py instman
	@echo "Installing fbak"
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp fbak.py $(EXEC)
	cp $(EXEC) $(DESTDIR)$(PREFIX)/bin/$(EXEC)
	chmod 555 $(DESTDIR)$(PREFIX)/bin/$(EXEC)

.PHONY: reinstall
reinstall: deinstall install

.PHONY: deinstall
deinstall:
	@echo "Uninstalling $(EXEC)"
	rm -f $(DESTDIR)$(PREFIX)/bin/$(EXEC)
	rm -f /usr/share/man/man1/fbak.1.gz

.PHONY: clean
clean:
	@rm -rf $(EXEC) $(EXEC).dist $(EXEC).build

.PHONY: cleanall
cleanall: clean
	@rm -rf *.BAK.*
