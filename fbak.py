#!/usr/bin/env python
"""
fbak is a script that creates time stampted backups of the given arguments
Author: Ricardo Castilla
"""

from __future__ import print_function
import argparse
from os import path
from os import chdir
from shutil import copy2
import datetime
import tarfile

def handle_dir(trgt, dst, compress, verb):
    """
    handle tarfile creation for one directory.
    src = string with source of directory
    dst = string with name of tar file
    compress = bool whether to use compression
    verb = bool whether to be verbose
    """
    tar_flags = "w"
    dst += ".tar"

    # delete last character
    if trgt[-1] == '/':
        trgt = trgt[:-1]
    # cannot use absolute paths
    if trgt[0] == '/':
        trgt = trgt[1:]

    if verb:
        print("creating {} tarball...".format("compressed" if compress else "uncompressed"))
    if compress:
        tar_flags += ":gz"
        dst += ".gz"
    with tarfile.open(dst, tar_flags) as new_tarfile:
        new_tarfile.add(trgt)

def main(targets, todir="", verb=False, compress=False):
    """
    main routine
    """
    resetdir = False
    for trgt in targets:
        # delete last character
        if trgt[-1] == '/':
            trgt = trgt[:-1]
        # if todir is empty, make it the target's directory
        if not todir:
            todir = path.dirname(trgt)
            resetdir = True # we will want to reset todir later
        # manipulate the data
        todir = path.expanduser(path.expandvars(todir))
        curr_timestamp = datetime.datetime.now().isoformat().replace(":", "-").replace(".", "-")
        trgt = path.expanduser(path.expandvars(trgt))
        dst = path.basename(trgt)+".BAK.{}".format(curr_timestamp)
        dst = path.join(todir, dst) # make sure is cross platform

        # TODO: Need to add try/except blocks to handle errors
        # determine what operation to do
        if path.isdir(trgt):
            handle_dir(trgt, dst, compress, verb)
        else:
            if verb:
                print("copying {} -> {}".format(trgt, dst))
            copy2(trgt, dst) # copy2 attempts to keep file metadata

        # reset todir to keep sanity
        if resetdir:
            todir = ""
            resetdir = False

def gui_handle(trgt):
    """
    handle path manipulation for gui usage
    """
    if trgt[-1] == '/':
        trgt = trgt[:-1]
    return path.basename(trgt)

if __name__ == "__main__":
    # parse command line arguments
    parser = argparse.ArgumentParser(description='Time stampted backup script')
    parser.add_argument('targets', metavar='TARGET', nargs='+',
                        help='Target to backup.')
    parser.add_argument('-m', metavar='todir', nargs=1, default='',
                        dest='todir',
                        help='Move backups to folder.')
    parser.add_argument('-n', '--gui-dir', metavar='curr_gui_dir', nargs=1,
                        dest='curr_dir',
                        help='Current directory in the Graphical Interface')
    parser.add_argument('-c', '--compress', dest='compress', action='store_true',
                        help='Use compression')
    parser.add_argument('-v', action='store_true', dest='verb',
                        help='Enable verbose')
    args = parser.parse_args()

    if isinstance(args.todir, list):
        args.todir = args.todir[0]
    if isinstance(args.curr_dir, list):
        args.curr_dir = args.curr_dir[0]

    # Change the current working directory to the one given by --gui_dir if given
    if args.curr_dir:
        # move to the given directory and replace change the name of the give targets
        # to not contain the path
        chdir(args.curr_dir)
        args.targets[:] = [gui_handle(src) for src in args.targets]

    main(args.targets, args.todir, args.verb, args.compress)
